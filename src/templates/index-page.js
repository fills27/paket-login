import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql } from 'gatsby'

import Layout from '../components/Layout'
import Features from '../components/Features'
import BlogRoll from '../components/BlogRoll'

import bg from '../img/bg.png'
import logo from '../img/logo-blue.png'
import logoWhite from '../img/logo.png'

import 'bootstrap/dist/css/bootstrap.css'

export const IndexPageTemplate = ({
  image,
  title,
  heading,
  subheading,
  mainpitch,
  description,
  intro,
}) => (
  <section className="h-100 w-100">
    <div className="container-fluid p-0 h-100">
      <div className="row h-100 no-gutters">
        <div className="col-md-5 mobile-none bg-left">
          <div className="d-flex align-items-center position-relative h-100">
            <div className="d-inline-block">
              <div className="pt-1">
                <div className="pt-5 w-100 px-5">
                  {/* <img className="img-fluid img-logo mb-3" src={logo}/> */}
                  <h5>
                    <b>
                      Your one stop platform to manage all of your field service management
                    </b>
                  </h5>
                </div>
              </div>
              <img className="img-fluid img-bg mb-5 mt-4" src={bg}/>
              <div className="px-5">
                <span>
                  © Copyright 2019 PT. Paket Informasi Digital. All Rights Reserved
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-7 bg-right">
          <div className="lang">
            <span className="text-primary">
              English
            </span>
          </div>
          <div className="row m-0 w-100 align-items-center h-100 justify-content-center">
            <div className="col-md-6 col-10 p-0">
              <img className="img-fluid img-logo mb-4" src={logo}/>
              <h5>
                <b>
                  Sign in to Mile App
                </b>
              </h5>
              <div className="input-place mt-5">
                <h6 className="mb-2">
                  <b>Organization's name</b>
                </h6>
                <input placeholder="Enter your organization's name" className="form-control mb-4"/>
                <button className="btn btn-primary mt-1 w-100">
                  Login
                </button>
                <span className="mt-4 additional-desc d-block">
                  Not registered yet? <a href="/contact">Contact us</a> for more info
                </span>
                <span className="d-block additional-desc">
                  Call Us Now: +62 812-1133-5608
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)

IndexPageTemplate.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
  heading: PropTypes.string,
  subheading: PropTypes.string,
  mainpitch: PropTypes.object,
  description: PropTypes.string,
  intro: PropTypes.shape({
    blurbs: PropTypes.array,
  }),
}

const IndexPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <Layout>
      <IndexPageTemplate
        image={frontmatter.image}
        title={frontmatter.title}
        heading={frontmatter.heading}
        subheading={frontmatter.subheading}
        mainpitch={frontmatter.mainpitch}
        description={frontmatter.description}
        intro={frontmatter.intro}
      />
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPageTemplate {
    markdownRemark(frontmatter: { templateKey: { eq: "index-page" } }) {
      frontmatter {
        title
        image {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        heading
        subheading
        mainpitch {
          title
          description
        }
        description
        intro {
          blurbs {
            image {
              childImageSharp {
                fluid(maxWidth: 240, quality: 64) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            text
          }
          heading
          description
        }
      }
    }
  }
`
